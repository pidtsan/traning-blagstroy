import './fullscreen-menu.mustache'
import './fullscreen-submenu.mustache'
import {Accordion} from "../accordion/accordion"
import '../contact/contact-phone.mustache'

export class FullscreenMenu {
  constructor(params) {
    this.params = $.extend(true,
      {
        selectors: {
          trigger: '.header-menu-button',
          header: '.header',
          closeButton: '.fullscreen-menu-close-button'
        },
        classes: {
          triggerMenuOpen: 'header-menu-button_opened',
          showFullscreenMenu: 'header_show-fullscreen-menu'
        }
      },
      params
    );

    this.init();
    this.menuOpened = false;
  }

  init() {
    this.attachEvents();

    if (isMobile()) {
      this.initMenuAccordions();

      $('.fullscreen-submenu:not(.fullscreen-submenu_opened)')
        .find('.fullscreen-submenu-list')
        .css('display', 'none');
    }
  }

  attachEvents() {
    this.attachTriggerEvent();
    this.attachEventToCloseButton();
  }

  attachTriggerEvent() {
    if ($(this.params.selectors.trigger).length > 0) {
      $(this.params.selectors.trigger).on('click', (function() {
        this.menuOpened = !this.menuOpened;
        $(this.params.selectors.header).toggleClass(this.params.classes.showFullscreenMenu);
        $(this.params.selectors.trigger).toggleClass(this.params.classes.triggerMenuOpen);
        $('body').toggleClass('page_no-scroll');

        if (this.menuOpened) {
          $('.page-menu-navigation-body').css('z-index', 0);
        }
        else {
          $('.page-menu-navigation-body').css('z-index', 9999);
        }
      }).bind(this));
    }
  }

  attachEventToCloseButton() {
    $(this.params.selectors.closeButton).on('click', (function() {
      $(this.params.selectors.header).removeClass(this.params.classes.showFullscreenMenu);
      $(this.params.selectors.trigger).removeClass(this.params.classes.triggerMenuOpen);
      $('body').removeClass('page_no-scroll');

      this.menuOpened = false;
      $('.page-menu-navigation-body').css('z-index', 9999);
    }).bind(this));
  }

  initMenuAccordions() {
    new Accordion({
      selectors: {
        accordion: ".fullscreen-submenu-wrapper",
        item: ".fullscreen-submenu",
        trigger: '.fullscreen-submenu__title',
        hidden: '.fullscreen-submenu-list'
      },
      classes: {
        opened: 'fullscreen-submenu_opened'
      },
      oneOpen: true,
    });
  }
}

export class CollapsedBlock {
  constructor(params) {
    this.params = $.extend(true, {
      selectors: {
        container: '.collapsed-block-container',
        wrapper: '.collapsed-block-wrapper',
        trigger: '.collapsed-block-trigger',
        triggerText: '.collapsed-block-trigger'
      },
      classes: {
        containerOpened: 'collapsed-block-container_open',
        blockHideTrigger: 'collapsed-block_hide-trigger'
      },
      settings: {
        triggerText: 'Читать полностью',
        triggerTextOpened: 'Свернуть'
      }
    },
      params);

    if (this.exists()) {
      this.init();
    }
  }

  exists() {
    return $(this.params.selectors.container).length > 0;
  }

  init() {
    this.opened = false;

    this.$container = this.getActualContainer();
    if(this.$container && this.$container.length) {
        this.$wrapper = this.getActualWrapper();

        this.attachEvents();

        setTimeout((function() {
          if (!this.needShowTrigger()) {
            this.hideTrigger();
          }
        }).bind(this), 50);
    }
  }

  getActualContainer() {
    let $item;

    $(this.params.selectors.container).each(function(index, item) {
      if ($(item).height() > 0) {
        $item = $(item);
      }
    });

    return $item;
  }

  getActualWrapper() {
    let $item;

    this.$container.find(this.params.selectors.wrapper).each(function(index, item) {
      if ($(item).height() > 0) {
        $item = $(item);
      }
    });

    return $item;
  }

  needShowTrigger() {
    return this.$container.height() < this.$wrapper.height();
  }

  hideTrigger() {
    let $collapsedBlock = this.$container.closest('.collapsed-block');

    $collapsedBlock.find(this.params.selectors.trigger).hide();
    $collapsedBlock.addClass(this.params.classes.blockHideTrigger);
  }

  showTrigger() {
    let $collapsedBlock = this.$container.closest('.collapsed-block');

    $collapsedBlock.find(this.params.selectors.trigger).show();
    $collapsedBlock.removeClass(this.params.classes.blockHideTrigger);
  }

  getWrapperHeight() {
    return this.$wrapper.height();
  }

  attachEvents() {
    this.$container.closest('.collapsed-block').find(this.params.selectors.trigger).on('click', (function() {
      this.toggle();

      if (this.opened) {
        this.expand();
      }
      else {
        this.collapse();
      }

    }).bind(this));

    $(window).on('resize', (function() {
      if (this.needShowTrigger()) {
        this.showTrigger();
      }
      else {
        this.hideTrigger();
      }
    }).bind(this));
  }

  toggle() {
    this.opened = !this.opened;
  }

  collapse() {
    $(this.params.selectors.triggerText).text(this.params.settings.triggerText);
    this.$container.removeAttr('style');
    this.$container.removeClass(this.params.classes.containerOpened);
  }

  expand() {
    $(this.params.selectors.triggerText).text(this.params.settings.triggerTextOpened);
    this.$container.css('max-height', this.getWrapperHeight());
    this.$container.addClass(this.params.classes.containerOpened);
  }
}

import './company-employee.mustache'
import Swiper from 'swiper'

export default () => {

  const breakpoint = window.matchMedia( '(min-width:768px)' );
  let mySwiper;

  const breakpointChecker = function() {
    if ( breakpoint.matches === true ) {
      if ( mySwiper !== undefined ) mySwiper.destroy(true, true);
      return;
    } else if ( breakpoint.matches === false ) {
      return enableSwiper();
    }
  };

  const enableSwiper = function() {
    let slides = $('.company-employee__slide').length;

    mySwiper = new Swiper('.company-employee', {
      spaceBetween: 10,
      pagination: {
        el: '.company-employee__pagination',
        type: 'bullets',
        dynamicBullets: slides > 3 ? true : false,
        clickable: true,
      },
      on: {
        slideChange: function () {
          bLazy.revalidate()
        },
      },
    })
  };

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();

}

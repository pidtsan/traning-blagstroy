import '../rubric/rubric.mustache'

import {Dropdown} from '../dropdown/dropdown'

export default () => {
  new Dropdown({
    changeItemActivity: true,
    preventLinkClick: false,
  });
}

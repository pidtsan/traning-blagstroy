export const demo = {
  'item': {
    coords: [50.30827961938753, 127.5059382539977],
    name: 'Тепличная, 1',
    image: '/local/assets/images/project-layout-rooms/room-1.png'
  },
  'balloons': [
    {
      name: 'Остановка Перинатальный центр',
      coords: [50.30770246211558, 127.50485464155571],
      category: 'stops',
      content: {
        header: 'Остановка Перинатальный центр',
        body: 'Маршруты:  2К, 2Кт, 4, 11, 24, 25, 30, 36, 36о, 44'
      }
    },
    {
      name: 'Перинатальный центр',
      coords: [50.307133482677855, 127.50290296007637],
      category: 'healthcare',
      content: {
        header: 'Перинатальный центр',
        body: 'Какой нибудь текст про перинатальный центр'
      }
    },
    {
      name: 'Областная больница',
      coords: [50.30562525013739, 127.5065357336742],
      category: 'healthcare',
      content: {
        header: 'Областная больница',
        body: 'Какой нибудь текст про областную больницу'
      }
    },
    {
      name: 'Fresh market',
      coords: [50.30905046523976, 127.50863215853217],
      category: 'markets',
      content: {
        header: 'Fresh market',
        body: 'Скоро здесь откроется новый Fresh market'
      }
    },
    {
      name: 'Школа №16',
      coords: [50.306109685968664, 127.5137176268244],
      category: 'schools',
      content: {
        header: 'Школа №16',
        body: 'Школа на 1000 человек'
      }
    },
    {
      name: 'ДОУ ДС №2',
      coords: [50.309524545717046, 127.50697991777898],
      category: 'kindergartens',
      content: {
        header: 'ДОУ ДС №2',
        body: 'Детский сад на 200 человек'
      }
    }
  ]
}

import otherProjects from "../other-projects/other-projects";
import './documentation.sass'
import documents from "../documents/documents";


$(document).ready(function() {
  documents();
  otherProjects();
});

import './article.sass'
import '../article-main/article-main.mustache'
import '../article-main/article-main__video.mustache'
import '../article-main/article-main__image.mustache'
import '../consultation-form/consultation-form.mustache'
import '../article-form/article-form.mustache'
import articleSlider from '../article-slider/article-slider'
import elseSlider from '../article-else/article-else'
import {indexText} from "../index-text/index-text"
import share from '../site-socials/site-socials'
import scrollup from '../scrollup/scrollup'
import {articleText} from "./article-text";

$(document).ready(function () {
  articleSlider();
  elseSlider();
  indexText();
  share();
  scrollup();
  articleText();
});

import './header-menu-list.mustache'
import {ListItemsHover} from "../list-items-hover/list-items-hover";

export class HeaderMenu {
  constructor() {
    this.params = {
      selectors: {
        header: '.header',
        mainMenuPart: '.header-container-wrapper-main',
        menuItem: '.header-menu-list-item',
        submenu: '.header-submenu-container',
        submenuBackground: '.header-submenu-background',
        searchButton: '.header-menu-search-button_desktop',
        searchInput: '.header-menu .header-search__textbox',
        searchButtonClose: '.header-menu-search-close',
        searchContainer: '.header-menu-search',
        screenOverlay: '.screen-overlay',
      },
      classes: {
        headerCompact: 'header_compact',
        submenuOpened: 'header-submenu-container_show',
        mainMenuPartHidden: 'header-container-wrapper_hidden',
        submenuBackgroundShow: 'header-submenu-background_show',
        searchContainerShow: 'header-menu-search_open',
        screenOverlayShow: 'screen-overlay_show'
      },
      settings: {
        offsetTop: 200
      }
    };

    this.init();
  }

  init() {
    this.initMainMenuHover();
    this.attachEvents();
    this.isMenuCompact = false;
  }

  initMainMenuHover() {
    if (isDesktop()) {
      new ListItemsHover({
        selectors: {
          container: '.header-menu-list-wrapper',
          listItem: '.header-menu-list-item',
          listItemText: '.header-menu-list-item__link-text'
        },
        classes: {
          listItemActive: 'header-menu-list-item_active'
        }
      });
    }
  }

  getSubmenu(item) {
    return $(item).find(this.params.selectors.submenu);
  }

  showSubmenu(item) {
    $(item).find(this.params.selectors.submenu).addClass(this.params.classes.submenuOpened);
  }

  hideSubmenu() {
    $(this.params.selectors.submenu).removeClass(this.params.classes.submenuOpened);
  }

  showSubmenuBackground(item) {
    $(this.params.selectors.submenuBackground)
      .css({
        'padding-top': this.getSubmenu(item).outerHeight()
      })
      .addClass(this.params.classes.submenuBackgroundShow);
  }

  hideSubmenuBackground() {
    $(this.params.selectors.submenuBackground)
      .removeAttr('style')
      .removeClass(this.params.classes.submenuBackgroundShow);
  }

  hasSubmenu(item) {
    return this.getSubmenu(item).length > 0;
  }

  enableMenuCompact() {
    let $menuPart = $(this.params.selectors.mainMenuPart);

    $menuPart.addClass(this.params.classes.mainMenuPartHidden);

    $(this.params.selectors.header).addClass(this.params.classes.headerCompact);
    this.isMenuCompact = true;
  }

  disableMenuCompact() {
    $(this.params.selectors.mainMenuPart)
      .removeAttr('style')
      .removeClass(this.params.classes.mainMenuPartHidden);

    $(this.params.selectors.header).removeClass(this.params.classes.headerCompact);
    this.isMenuCompact = false;
  }

  closeHeaderMenu() {
    $(this.params.selectors.searchContainer).removeClass(this.params.classes.searchContainerShow);
    $(this.params.selectors.screenOverlay).removeClass(this.params.classes.screenOverlayShow);
  }

  attachEvents() {
    $(this.params.selectors.menuItem).hover(

      (function(event) {
        if (this.hasSubmenu(event.currentTarget)) {
          this.showSubmenuBackground(event.currentTarget);
          this.showSubmenu(event.currentTarget);
        }
      }).bind(this),

      (function() {
        this.hideSubmenuBackground();
        this.hideSubmenu();
      }).bind(this)
    );

    $(window).on('scroll', (function() {
      if ($(window).scrollTop() > this.params.settings.offsetTop) {
        if (!this.isMenuCompact) this.enableMenuCompact();
      }
      else {
        if (this.isMenuCompact) this.disableMenuCompact();
      }
    }).bind(this));

    $(this.params.selectors.searchButton).on('click', (function() {
      $(this.params.selectors.searchContainer).addClass(this.params.classes.searchContainerShow);
      $(this.params.selectors.screenOverlay).addClass(this.params.classes.screenOverlayShow);
      $(this.params.selectors.searchInput).focus();
    }).bind(this));

    $(this.params.selectors.searchButtonClose).on('click', this.closeHeaderMenu.bind(this));
    $(this.params.selectors.screenOverlay).on('click', this.closeHeaderMenu.bind(this));
  }
}

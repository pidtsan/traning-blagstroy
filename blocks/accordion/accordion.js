export class Accordion {
    constructor(params) {
        this.params = $.extend(true, {
                selectors: {
                    accordion: ".accordion", // контейнер для всех вкладок
                    item: ".accordion-item",    // вкладка или элемент аккордиона
                    trigger: '.accordion__trigger', // область переключения состояний аккордиона
                    hidden: '.accordion-hidden' // скрываемая/раскрываемая область
                },
                classes: {
                    opened: 'accordion-hidden_opened'   // класс, который вешается на вкладку и отмечает открытое состояние
                },
                _active: true,  // активность плагина
                _slideSpeed: 400,   // скорость анимации
                oneOpen: false, // режим, когда открыт только один элемент, остальные свернуты
                initialItem: false, // первая раскрыта по умолчанию
                preventDefault: false,  // запрет браузерных действия по умолчанию, для триггера (нужно, если триггер - сслыка </a>)
                onExpand: false,    // обработчик события раскрытия
                onCollapse: false,  // обработчик события сокрытия
                onInit: false,  // обработчик события инициализации
                expandCallback: undefined, // функция, выполняющаяся после разворачивания элемента
                collapseCallback: undefined, // функция, выполняющаяся после сворачивания элемента
            },
            params);

        this.init();
    }

    init () {
        this.$accordion = $(this.params.selectors.accordion);
        this.attachEvents();

        if (this.params.initialItem) {
            $(`${this.params.selectors.trigger}:first`).trigger('click');
        }

        if (this.params.onInit && typeof this.params.onInit === 'function') {
            this.params.onInit(this.$accordion, this.params);
        }
    }

    attachEvents () {
        if (this.params._active) {
            this.$accordion.on('click', this.params.selectors.trigger, this.triggerClickHandler.bind(this));
        }
    }

    triggerClickHandler (event) {
        if (this.params.preventDefault) {
            event.preventDefault();
        }

        if (this.params._active) {
            let curTrigger = $(event.target),
                curItem = curTrigger.closest(this.params.selectors.item);

            if (this.params.oneOpen) {
                let self = this,
                    $accordion = curTrigger.closest(this.params.selectors.accordion),
                    $items = $accordion.find(this.params.selectors.item);

                $items.each((index, item) => {
                    if (item !== curItem[0]) {
                        this.collapse(event, $(item));
                    }
                });
            }

            if (curItem.hasClass(this.params.classes.opened)) {
                this.collapse(event, curItem);
            } else {
                this.expand(event, curItem);
            }
        }
    }

    expand (event, item) {
        let hidden = item.find(this.params.selectors.hidden);

        hidden.stop(true).slideDown({
            duration: this.params._slideSpeed,
            complete: this.params.expandCallback
        });
        item.addClass(this.params.classes.opened);

        bLazy.revalidate();

        setTimeout(() => {
            item.trigger('expanded');
        }, this.params._slideSpeed);

        if (this.params.onExpand && typeof this.params.onExpand === 'function') {
            this.params.onExpand(event, item);
        }
    }

    collapse (event, item) {
        let hidden = item.find(this.params.selectors.hidden);

        hidden.stop(true).slideUp({
            duration: this.params._slideSpeed,
            complete: this.params.collapseCallback
        });
        item.removeClass(this.params.classes.opened);

        setTimeout(() => {
            item.trigger('collapsed');
        }, this.params._slideSpeed);

        if (this.params.onCollapse && typeof this.params.onCollapse === 'function') {
            this.params.onCollapse(event, item);
        }
    }

    collapseAll($accordion) {
        $accordion.find(this.params.selectors.hidden).stop(true).slideUp();
        $accordion.find(this.params.selectors.item).removeClass(this.params.classes.opened);
    }
}

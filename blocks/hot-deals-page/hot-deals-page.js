import './hot-deals-page.sass'
import {InfinityNav} from "../infinite/infinite";
import {IndexProducts} from "../index-products/index-products";
import otherProjects from "../other-projects/other-projects";
import {CollapsedBlock} from "../collapsed-block/collapsed-block";

$(document).ready(function () {
  IndexProducts();
  otherProjects();
  new InfinityNav;
  bLazy.revalidate();
  new CollapsedBlock();
});

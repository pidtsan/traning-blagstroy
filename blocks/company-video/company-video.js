import './company-video.mustache'
import Swiper from 'swiper'

export default () => {

  let slides = $('.company-employee__slide').length;
  let mySwiper = new Swiper('.company-video__slider', {
    spaceBetween: 10,
    pagination: {
      el: '.company-video__slider-pagination',
      type: 'bullets',
      dynamicBullets: slides > 3 ? true : false,
      clickable: true,
    },
    on: {
      slideChange: function () {
        bLazy.revalidate()
      },
    },
  })

  $('.company-video__preview-item').on('click', function () {
    $('.company-video__preview-item_active').removeClass('company-video__preview-item_active');
    $(this).addClass('company-video__preview-item_active');
    let number = $(this).data("number");
    mySwiper.slideTo(number, 300, true);
  })

  mySwiper.on('slideChange', function() {
    let number = mySwiper.activeIndex;
    let preview = $('.company-video__preview-item');
    $('.company-video__preview-item_active').removeClass('company-video__preview-item_active');

    for (let i = 0 ; i < preview.length ; i++) {
      if ($(preview[i]).data('number') === number) {
        $(preview[i]).addClass('company-video__preview-item_active')
      }
    }
  });
}


"use strict";

import './index.sass';
import {indexFirstScreen} from "../index-first-screen/index-first-screen";
import {indexProjects} from "../index-projects/index-projects";
import '../svg/background-rectangles.mustache'
import '../svg/background-circle-dashed.mustache'
import '../svg/background-lines.mustache'
import '../index-tour-form/index-tour-form.mustache'
import '../tour-form/tour-form.mustache'
import '../article-form/article-form.mustache'
import '../consultation-form/consultation-form.mustache'
import 'svg/background-dots.mustache'
import 'svg/background-dots-line.mustache'
import {IndexProducts} from "../index-products/index-products";
import {buyWays} from "../buy-ways/buy-ways";
import {announcements} from "../announcements/announcements";
import {subscribeForm} from "../subscribe-form/subscribe-form";
import {partnersList} from "../partners-list/partners-list";
import {indexText} from "../index-text/index-text";
import {indexAbout} from "../index-about/index-about";

$(document).ready(function () {
    indexFirstScreen();
    indexProjects();
    IndexProducts();
    buyWays();
    announcements();
    subscribeForm();
    indexAbout();
    partnersList();
    indexText();
});

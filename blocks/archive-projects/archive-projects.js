import './archive-projects.sass'

import '../archive-item/archive-item.mustache'

import hotDeals from '../hot-deals/hot-deals'
import {InfinityNav} from '../infinite/infinite'
import archiveList from "../archive-list/archive-list";

$(document).ready(function () {
  archiveList();
  hotDeals();
  new InfinityNav;
});

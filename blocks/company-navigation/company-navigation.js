import './company-navigation.mustache'
import TimelineMax from 'TweenMax.min'

export default () => {
  const contentMargin = 104
  const topOffset = 79;// высота шапки для десктопа

  let points = $('.point');
  let sections = [];
  points.each((index, element) => {
    let href = $(element).attr('href');
    sections[index] = Math.floor($(href).offset().top - topOffset)
  });

  $('.company-navigation').addClass('company-navigation_showed')

  $('.point').on('click', function(event) {
    event.preventDefault();

    let href = $(event.currentTarget).attr('href');

    if (href === '#') return;

    if ($(href).length > 0) {
      let destinationPos = $(href).offset().top - topOffset;
      destinationPos = destinationPos === contentMargin ? 0 : destinationPos

      $("html, body").animate({
        scrollTop: destinationPos
      },500);
    }
  });

  let scrollPos = $(window).scrollTop();

  $(window).scroll(function(){
    let scrollTop = $(window).scrollTop();
    let currentIndex = 0
    let active = sections[0];
    let getTotalPoints = sections.length;

    for (let i = 0 ; i < sections.length ; i++) {
      active = sections[i];

      if (scrollTop >= sections[i]) {
        currentIndex = i;
      }
    }
    TweenMax.to($('.bar__fill'), 0.6, {
      height: currentIndex / (getTotalPoints - 1) * 100 + '%'
    });

    $('.point--active').removeClass('point--active');
    $(points[currentIndex]).addClass('point--active');

    if (scrollTop >= scrollPos){
      $(points[currentIndex]).prevAll().addClass('point--complete');
    } else {
      $(points[currentIndex]).nextAll().removeClass('point--complete');
    }
    scrollPos = scrollTop;
  });
}

<?php

namespace ZLabs\BxMustache\Breadcrumbs;

use Illuminate\Support\Collection;

class Link extends \ZLabs\BxMustache\Link
{
    /** @var bool */
    public $isLast = false;
    /** @var bool */
    public $isLastButOne = false;
    /** @var int */
    public $index;
    /** @var Collection */
    public $items;
}

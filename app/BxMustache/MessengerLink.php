<?php

namespace ZLabs\BxMustache;

use ZLabs\BxMustache\Svg;

class MessengerLink extends Link
{
    /**@var Svg*/
    public $icon;
}
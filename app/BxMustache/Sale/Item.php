<?php

namespace ZLabs\BxMustache\Sale;

use ZLabs\BxMustache\DetailPageLink;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\IdTrait;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\ItemInterface;

class Item implements ItemInterface
{
    use IdTrait;
    use HermitageTrait;
    use DetailPageLink;

    /** @var int */
    public $categoryId;
    /** @var Image */
    public $image;
    /** @var float */
    public $square;
    /** @var float */
    public $price;
    /** @var float */
    public $fullPrice;
    /** @var float */
    public $discount;
    /** @var string */
    public $address;

    public function withSale()
    {
        return $this->discount > 0;
    }

    public function price()
    {
        if ($this->price > 0) {
            return $this->formatPrice($this->calculatePrice($this->price));
        }

        return '';
    }

    public function fullPrice()
    {
        if ($this->fullPrice > 0) {
            return $this->formatPrice($this->calculatePrice($this->fullPrice));
        }

        return '';
    }

    public function oldFullPrice()
    {
        if ($this->fullPrice > 0) {
            return $this->formatPrice($this->fullPrice);
        }

        return '';
    }

    protected function calculatePrice($price)
    {
        if ($this->withSale()) {
            return $price / 100 * (100 - $this->discount);
        }

        return $price;
    }

    protected function formatPrice($price)
    {
        $price = number_format($price, 0, '.', ' ');

        return "$price ₽";
    }
}

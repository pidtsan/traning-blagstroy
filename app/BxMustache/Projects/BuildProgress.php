<?php

namespace ZLabs\BxMustache\Projects;

use Illuminate\Support\Collection;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\IdTrait;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Video\Link;

class BuildProgress implements ItemInterface
{
    use IdTrait;
    use HermitageTrait;

    /** @var string */
    public $quarter;
    /** @var string */
    public $date;
    /** @var Collection */
    public $images;
    /** @var Collection */
    public $videos;

    public function firstLink()
    {
        return $this->links()->first();
    }

    public function linksWithoutFirst()
    {
        $links = $this->links();

        if ($links->count() > 1) {
            return $links->slice(1, $links->count() - 1)->values();
        }

        return new Collection;
    }

    public function links()
    {
        return $this->images
            ->map(function (Image $image) {
                return $image->realSrc;
            })
            ->merge(
                $this->videos
                    ->map(function (Link $link) {
                        return $link->href;
                    })
            )
            ->values();
    }
}

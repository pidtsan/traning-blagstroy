<?php

namespace ZLabs\BxMustache\Projects;

use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\IdTrait;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\DetailPageLink;

class Item implements ItemInterface
{
    use IdTrait;
    use HermitageTrait;
    use DetailPageLink;

    /** @var bool */
    public $isBig = true;
    /** @var AdaptiveImage */
    public $image;
    /** @var string */
    public $area;
    /** @var string */
    public $fullPrice;
    /** @var string */
    public $price;
    /** @var string */
    public $address;
    /** @var string */
    public $deadline;
    /** @var string */
    public $anotherFooterText;

    public function hasPrice()
    {
        return $this->price || $this->fullPrice;
    }
}

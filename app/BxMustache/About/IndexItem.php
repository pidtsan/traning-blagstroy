<?php

namespace ZLabs\BxMustache\About;

use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Link;

class IndexItem implements ItemInterface
{
    use HermitageTrait;
    /**@var string*/
    public $cardTitle;
    /**@var string*/
    public $text;
    /**@var Link*/
    public $link;
    /**@var Image*/
    public $image;
}

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: главная страница</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/index/index.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
<div class="index">
    <?php
    echo $mustache->render(
        'index-first-screen',
        include(CONTEXT_DIR . '/index/index-first-screen.php')
    );
    ?>
    <section class="index__section-projects page-section">
        <div class="container">
            <div class="index__section-projects__pattern-dots">
                <?php
                    echo $mustache->render('background-dots');
                ?>
            </div>
            <div class="index__section-projects__pattern-circle">
                <?php
                    echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <?php
            echo $mustache->render(
                'index-projects',
                include(CONTEXT_DIR . '/index/index-projects.php')
            );
            ?>
        </div>
    </section>

    <section class="index__section-tour-form container">
        <div class="pattern-dots pattern-dots_1">
            <?php
            echo $mustache->render('background-dots');
            ?>
        </div>
        <div class="pattern-dots pattern-dots_2">
            <?php
            echo $mustache->render('background-dots');
            ?>
        </div>
        <?php
            echo $mustache->render('index-tour-form', include(CONTEXT_DIR . 'index/tour-form-normal.php'));
        ?>
    </section>

    <section class="index__section-products page-section">
        <div class="container">
            <div class="background-pattern background-pattern_circle">
                <?php
                    echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <div class="background-pattern background-pattern_rectangles">
                <?php
                    echo $mustache->render('background-rectangles');
                ?>
            </div>
            <?php
            echo $mustache->render(
                'index-products',
                include(CONTEXT_DIR . '/index/index-products.php')
            );
            ?>
        </div>
    </section>

    <section class="index__section-buy-ways page-section">
        <div class="index__section-buy-ways-pattern">
            <?php
                echo $mustache->render('background-circle-dashed');
            ?>
        </div>
        <?php
        echo $mustache->render(
            'buy-ways',
            include(CONTEXT_DIR . 'index/buy-ways.php')
        );
        ?>
    </section>

    <section class="index__section-announcements page-section">
        <div class="container">
            <div class="background-pattern background-pattern_dots-line">
                <?php
                    echo $mustache->render('background-dots-line');
                ?>
            </div>
            <h3 class="announcements__title section-title">Акции и новости</h3>
            <?php
            echo $mustache->render('announcements-filter', include(CONTEXT_DIR . 'index/announcements-filter.php'));
            ?>
            <div class="announcements-section-wrapper">
                <div class="announcements">
                    <div class="announcements-content">
                        <?php
                        echo $mustache->render('announcements-content', include(CONTEXT_DIR . 'index/announcements-content.php'));
                        ?>
                    </div>
                    <a class="show-more-link"
                       href="#"
                       title="Показать все новости и статьи">Все публикации</a>
                </div>
                <div class="announcements_subscribe-form">
                    <?php
                    echo $mustache->render('subscribe-form', include(CONTEXT_DIR . 'index/subscribe-form.php'));
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="index__section-about page-section">
        <?php
        echo $mustache->render('index-about', include(CONTEXT_DIR . 'index/index-about.php'));
        ?>
    </section>

    <section class="index__section-partners page-section">
        <div class="container">
            <div class="background-pattern background-pattern_circle">
                <?php
                    echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <h3 class="section-title">Наши партнеры</h3>
            <?php
                echo $mustache->render('partners-list', include(CONTEXT_DIR . '/index/partners-list.php'));
            ?>
        </div>
    </section>

    <?php
        echo $mustache->render('consultation-form', include(CONTEXT_DIR . '/article/article-form.php'));
    ?>
    <?php
        echo $mustache->render('index-text', [
            'text' => '<h2>Коммерческая и жилая недвижимость в Благовещенске</h2>
            <p>Ключевой набор SEO текста для повышения видимости страниц сайта. Цель - сделать так, чтобы поисковые роботы Яндекса и Google начали находить по запросам пользователей страницы именно вашего сайта. Безусловно, сбор ключевых слов (составление семантики) — первый шаг к этой цели.
            Дальше набрасывается условный «скелет» для распределения ключевых слов по разным посадочным страницам. А затем уже пишутся и внедряются статьи/метатеги.</p> 
            
            <p>Рынок строительства всегда был довольно насыщенным и переполнен предложениями, как транспорт в час пик. И в то же время, в этой сфере крутятся большие деньги. Хотите догнать конкурентов и встать впереди колонны – оригинальный продающий текст о строительной компании будет той самой лопатой, которой можно будет грести большие чеки заключённых договоров. Изюминка или фишка в рекламном тексте Вашей компании – это билет в первый ряд высоких продаж.</p> 
            <p>Рынок строительства всегда был довольно насыщенным и переполнен предложениями, как транспорт в час пик. И в то же время, в этой сфере крутятся большие деньги. Хотите догнать конкурентов и встать впереди колонны – оригинальный продающий текст о строительной компании будет той самой лопатой, которой можно будет грести большие чеки заключённых договоров. Изюминка или фишка в рекламном тексте Вашей компании – это билет в первый ряд высоких продаж.</p>
        ']);
    ?>
</div>
</div>

<?php require_once 'footer.php'; ?>

<script id="popup-calendar-template" type="x-tmpl-mustache">
  <?php include($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/popup-calendar.mustache') ?>
</script>

<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#bell-form').feedbackForm();
    });
    window.initFeedback.push(function () {
        $('#index-tour-form').feedbackForm();
    });
    window.initFeedback.push(function () {
        $('#subscribe-form').feedbackForm();
    });
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/index/index.js" defer></script>
</body>
</html>

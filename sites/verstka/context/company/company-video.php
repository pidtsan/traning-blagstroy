<?php

use ZLabs\BxMustache\Company\VideoItem;
use ZLabs\BxMustache\Video\Link;
use ZLabs\BxMustache\AdaptiveImage;

$items = collect([
    [
        'src' => '/local/assets/images/company-video/image1-dt.jpg',
        'md' => '/local/assets/images/company-video/image1-tablet.jpg',
        'lg' => '/local/assets/images/company-video/image1.jpg',
        'title' => "Открытие детского сада в Усть Ивановке",
        'link' => "https://www.youtube.com/watch?v=CMouZ55oOB0"
    ],
    [
        'src' => '/local/assets/images/company-slider-second/img-1-dt.jpg',
        'md' => '/local/assets/images/company-slider-second/img-1.jpg',
        'lg' => '/local/assets/images/company-slider-second/img-1-tab.jpg',
        'title' => "Открытие детского сада в Усть Ивановке",
        'link' => "https://www.youtube.com/watch?v=CMouZ55oOB0"
    ],
])->map(function ($arItem, $key) {
    $item = new VideoItem();

    $item->image = new AdaptiveImage();
    $item->image->src = $arItem['src'];
    $item->image->lgSrc = $arItem['lg'];
    $item->image->mdSrc = $arItem['md'];

    $item->link = new Link();
    $item->link->text = $arItem['title'];
    $item->link->href = $arItem['link'];

    $item->number = $key;

    return $item;
});

return [
    "title" => "Открытие детского сада в Усть Ивановке",
    "text" => "15 января в Амурской области открыл свои двери для маленьких
                гостей новый детский сад в с. Усть-Ивановка.",
    "items" => $items
];

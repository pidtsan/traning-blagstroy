<?php
use ZLabs\BxMustache\Contacts\SimpleItem;
use ZLabs\BxMustache\Phone;
use ZLabs\BxMustache\Link;

$items = collect([
    [
        'name' => 'Отдел снабжения',
        'phones' => [
            '<b>+7 (4162)</b>  52-76-55',
            '<b>+7 (4162)</b> 52-38-17'
        ],
        'workMode' => 'пн-пт с 8:00-17:00 перерыв с 12:00-13:00',
        'email' => 'pay@blagstroy.ru'
    ],
    [
        'name' => 'Бухгалтерия',
        'phones' => [
            '<b>+7 (4162)</b>  52-76-55',
        ],
        'workMode' => 'пн-пт с 8:00-17:00 перерыв с 12:00-13:00',
        'email' => 'только по телефону'
    ],
    [
        'name' => 'ЮРИДИЧЕСКИЙ ОТДЕЛ',
        'phones' => [
            '<b>+7 (4162)</b>  52-76-55',
            '<b>+7 (4162)</b> 52-38-17'
        ],
        'workMode' => 'пн-пт с 8:00-17:00 перерыв с 12:00-13:00',
        'email' => 'pay@blagstroy.ru'
    ],
    [
        'name' => 'ОТДЕЛ КАДРОВ',
        'phones' => [
            '<b>+7 (4162)</b>  52-76-55',
        ],
        'workMode' => 'пн-пт с 8:00-17:00 перерыв с 12:00-13:00',
        'email' => 'только по телефону'
    ],
    [
        'name' => 'ДИСПЕТЧЕР',
        'phones' => [
            '<b>+7 (4162)</b>  52-76-55',
            '<b>+7 (4162)</b> 52-38-17'
        ],
        'workMode' => 'пн-пт с 8:00-17:00 перерыв с 12:00-13:00',
        'email' => 'pay@blagstroy.ru'
    ],
    [
        'name' => 'ПРОИЗВОДСТВЕННО-ТЕХНИЧЕСКИЙ ОТДЕЛ',
        'phones' => [
            '<b>+7 (4162)</b>  52-76-55',
        ],
        'workMode' => 'пн-пт с 8:00-17:00 перерыв с 12:00-13:00',
        'email' => 'только по телефону'
    ],
    [
        'name' => 'ОТДЕЛ КАПИТАЛЬНОГО СТРОИТЕЛЬСТВА',
        'phones' => [
            '<b>+7 (4162)</b>  52-76-55',
        ],
        'workMode' => 'пн-пт с 8:00-17:00 перерыв с 12:00-13:00',
        'email' => 'только по телефону'
    ],
])->map(function ($arItem){
    $item = new SimpleItem();
    $item->strMainId = rand(100, 1000);
    $item->name = $arItem['name'];
    $item->phones = collect($arItem['phones'])->map(function ($phone){
        $item = new Phone();
        $item->value = $phone;
        return $item;
    });
    $item->workMode = $arItem['workMode'];
    $item->email = new Link();
    if(strpos($arItem['email'], "@")) {
        $item->email->href = $arItem['email'];
        $item->email->text = "Написать письмо";
    }else{
        $item->email->text = $arItem['email'];
    }
    return $item;
});


return $items;
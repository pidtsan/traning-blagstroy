<?php

use ZLabs\BxMustache\Breadcrumbs\Link;

return collect([
    [
        'href' => '#',
        'text' => 'Главная'
    ],
    [
        'href' => '#',
        'text' => 'Статьи и новости',
        'isLast' => true
    ],
])->map(function ($arBreadcrumbLink, $key) {
    $link = new Link;

    $link->index = $key + 1;
    $link->href = $arBreadcrumbLink['href'];
    $link->text = $arBreadcrumbLink['text'];

    if (isset($arBreadcrumbLink['isLast'])) {
        $link->isLast = $arBreadcrumbLink['isLast'];
    }

    return $link;
});

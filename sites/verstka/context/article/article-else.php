<?php

use ZLabs\BxMustache\Blog\Item;
use ZLabs\BxMustache\AdaptiveImage;
use \ZLabs\BxMustache\Link;

$items = collect([
    [
        'name' => "Помощь в оформление ипотеки для многодетных семей",
        'text' => 'Ипотека под материнский капитал на 2 100 000 руб. на 10 лет. 31 июля 2019 года вступают в силу 
                   изменения, внесенные в федеральный закон №218-ФЗ «О государственн',
        'category' => 'Новости',
        'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
        'srcMd' => '/local/assets/images/blog-item/blog-item1.jpg',
        'srcLg' => '/local/assets/images/blog-item/blog-item1.jpg',
        'link' => '#',
        'views' => '262',
        'date' => '28 апреля 2020',
        'isSale' => true
    ],
    [
        'name' => "В России упростились покупка и продажа недвижимости",
        'text' => '31 июля 2019 года вступают в силу изменения, внесенные в федеральный закон №218-ФЗ «О государственной регистрации недвижимости».
                Согласно нововведению, с 31 июля 2019
                31 июля 2019 года вступают в силу изменения, внесенные в федеральный закон №218-ФЗ «О государственной регистрации недвижимости».
                Согласно нововведению, с 31 июля 2019',
        'category' => 'Новости',
        'src' => '/local/assets/images/blog-item/blog-item2-dt.jpg',
        'srcMd' => '/local/assets/images/blog-item/blog-item2.jpg',
        'srcLg' => '/local/assets/images/blog-item/blog-item2.jpg',
        'link' => '#',
        'views' => '262',
        'date' => '28 апреля 2020',
        'isSale' => false
    ],
    [
        'name' => "Помощь в оформление ипотеки для многодетных семей",
        'text' => 'С 31 июля вступают в силу изменения в закон о государственной регистрации недвижимости.
                   внесенные в федеральный закон №218-ФЗ «О государствен юля 2019 года вступают в силу.',
        'category' => 'Новости',
        'src' => false,
        'srcMd' => false,
        'srcLg' => false,
        'link' => '#',
        'views' => '262',
        'date' => '28 апреля 2020',
        'isSale' => false
    ],
    [
        'name' => "Помощь в оформление ипотеки для многодетных семей",
        'text' => 'Ипотека под материнский капитал на 2 100 000 руб. на 10 лет. 31 июля 2019 года вступают в силу 
                   изменения, внесенные в федеральный закон №218-ФЗ «О государственн',
        'category' => 'Новости',
        'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
        'srcMd' => '/local/assets/images/blog-item/blog-item1.jpg',
        'srcLg' => '/local/assets/images/blog-item/blog-item1.jpg',
        'link' => '#',
        'views' => '262',
        'date' => '28 апреля 2020',
        'isSale' => true
    ],
    [
        'name' => "В России упростились покупка и продажа недвижимости",
        'text' => 'Ипотека под материнский капитал на 2 100 000 руб. на 10 лет. 31 июля 2019 года вступают в силу 
                   изменения, внесенные в федеральный закон №218-ФЗ «О государственн',
        'category' => 'Новости',
        'src' => '/local/assets/images/blog-item/blog-item2-dt.jpg',
        'srcMd' => '/local/assets/images/blog-item/blog-item2.jpg',
        'srcLg' => '/local/assets/images/blog-item/blog-item2.jpg',
        'link' => '#',
        'views' => '262',
        'date' => '28 апреля 2020',
        'isSale' => false
    ],
])->map(function ($arItem){
    $item = new Item();

    $item->strMainId = rand(100, 1000);
    $item->title = $arItem['name'];
    $item->text = $arItem['text'];
    $item->category = "#".$arItem['category'];

    $item->detailPageLink = new Link;
    $item->detailPageLink->href = $arItem['link'];
    $item->detailPageLink->text = $arItem['name'];

    $item->image = new AdaptiveImage();
    $item->image->src = $arItem['src'];
    $item->image->mdSrc = $arItem['srcMd'];
    $item->image->lgSrc = $arItem['srcLg'];

    $item->views = $arItem['views'];
    $item->date = $arItem['date'];
    $item->isSale = $arItem['isSale'];

    return $item;
});

return [
    'title' => 'Почитать еще',
    'items' => $items
];
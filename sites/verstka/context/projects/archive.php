<?php

use ZLabs\BxMustache\Link;

$link = new Link;
$link->href = '#';
$link->text = 'Все проекты';

return [
    'title' => 'Архив проектов',
    'items' => include(__DIR__ . '/../archive-projects.php'),
    'link' => $link
];

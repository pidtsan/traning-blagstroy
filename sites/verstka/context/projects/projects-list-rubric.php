<?php

use ZLabs\BxMustache\Menu\Item;

return [
    'needShowDropdown' => true,
    'categories' => collect([
        [
            'title' => 'Жилые дома',
            'active' => true,
            'link' => '#',
        ],
        [
            'title' => 'Коммерческая недвижимость',
            'active' => false,
            'link' => '#',
        ],
        [
            'title' => 'Гаражи и машиноместа',
            'active' => false,
            'link' => '#',
        ]
    ])->map(function ($arItem) {
        $item = new Item;

        $item->text = $arItem['title'];
        $item->href = $arItem['link'];
        $item->active = $arItem['active'];

        return $item;
    })
];

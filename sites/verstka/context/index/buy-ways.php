<?php

use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\PaymentMethods\Item;
use ZLabs\BxMustache\Svg;

return [
    'title' => 'Способы покупки',
    'methods' => collect([
        [
            'icon' => '/local/assets/images/temp/buying-methods/icon-1.svg',
            'name' => 'Материнский капитал',
            'text' => 'Вы получили или планируете в ближайшее время получить <b>материнский капитал?</b>'
        ],
        [
            'icon' => '/local/assets/images/temp/buying-methods/icon-1.svg',
            'name' => 'Военная ипотека',
            'text' => 'Оперативное решение квартирного вопроса для <b>военнослужащих</b>'
        ],
        [
            'icon' => '/local/assets/images/temp/buying-methods/icon-1.svg',
            'name' => 'Беспроцентная рассрочка',
            'text' => '0% переплаты до сдачи дома. Получите рассрочку на  недвижимость от <b>«Благовещенскстрой»</b>'
        ],
        [
            'icon' => '/local/assets/images/temp/buying-methods/icon-1.svg',
            'name' => 'Государственные жилищные сертификаты',
            'text' => 'Поможем продать старую квартиру и купить новую в'
        ]
    ])->map(function ($arItem, $key) {
        $item = new Item;

        $item->id = $key + 1;
        $item->strMainId = $key + 1;
        $item->icon = new Svg;
        $item->icon->src = $arItem['icon'];
        $item->name = $arItem['name'];
        $item->text = $arItem['text'];
        $item->link = new Link;
        $item->link->href = '#';
        $item->link->text = 'Подробнее';

        return $item;
    })
];

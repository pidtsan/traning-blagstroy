<?php

use Illuminate\Support\Collection;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\Menu\Item;

/** @var Collection $categories */
$categories = include(__DIR__ . '/../projects/projects-list-rubric.php');
$categories = $categories['categories']->map(function (Item $item, $key) {
    return [
        'id' => $key + 1,
        'href' => $item->href,
        'name' => $item->text,
        'active' => $key === 0
    ];
});
/** @var Collection $items */
$items = include(__DIR__ . '/../projects.php');

$link = new Link;
$link->href = '#';
$link->text = 'Все проекты';

return collect([
    'title' => 'Проекты компании',
    'categories' => $categories,
    'needShowDropdown' => true,// ключ, который отвечает за показ выпадающего списка
    'items' => $categories->map(function ($arItem) use ($items) {
        return [
            'id' => $arItem['id'],
            'active' => $arItem['active'],
            'items' => $items->chunk(2)
        ];
    }),
    'link' => $link
]);

<?php

use ZLabs\BxMustache\Menu\Item;

return collect([
    [
        'title' => 'Проекты',
        'opened' => true,
        'items' => collect([
            [
                'href' => '#',
                'text' => 'Жилые комплексы',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Коммерческие помещения',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Гаражи и машиноместа',
                'active' => true
            ]
        ])
    ],
    [
        'title' => 'Клиентам',
        'opened' => false,
        'items' => collect([
            [
                'href' => '#',
                'text' => 'Способы покупки',
                'active' => false,
                'targetBlank' => false
            ],
            [
                'href' => '#',
                'text' => 'Блог',
                'active' => 'Ипотека'
            ],
            [
                'href' => '#',
                'text' => 'Рассрочка',
                'active' => true
            ],
            [
                'href' => '#',
                'text' => 'Информация для собственников',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Региональный покупатель',
                'active' => false
            ]
        ])
    ],
    [
        'title' => 'Информация',
        'opened' => false,
        'items' => collect([
            [
                'href' => '#',
                'text' => 'О компании',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Контакты',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Инвесторам',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Новости',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Вакансии',
                'active' => false
            ],
            [
                'href' => '#',
                'text' => 'Офисы продаж',
                'active' => false
            ]
        ])
    ]
])->map(function ($arColumn) {
    $arColumn['items'] = $arColumn['items']->map(function ($arItem) {
        $link = new Item;
        $link->href = $arItem['href'];
        $link->text = $arItem['text'];
        $link->active = $arItem['active'];

        return $link;
    });

    return $arColumn;
});

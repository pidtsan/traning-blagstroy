<?php

return collect([
    [
        'id' => 1,
        'href' => '#',
        'name' => 'Офисы',
        'active' => true
    ],
    [
        'id' => 2,
        'href' => '#',
        'name' => 'Производство'
    ],
    [
        'id' => 3,
        'href' => '#',
        'name' => 'Торговая площадь'
    ],
    [
        'id' => 4,
        'href' => '#',
        'name' => 'Склады'
    ]
]);

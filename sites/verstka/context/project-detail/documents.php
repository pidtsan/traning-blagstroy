<?php

use Illuminate\Support\Collection;
use ZLabs\BxMustache\Documents\Document;

return [
    'title' => 'Документация',
    'needShowDropdown' => true,// ключ, который отвечает за показ выпадающего списка
    'categories' => collect([
        [
            'id' => 1,
            'name' => 'Разрешения',
            'active' => true
        ],
        [
            'id' => 2,
            'name' => 'Отчетность',
            'active' => false
        ],
        [
            'id' => 3,
            'name' => 'Сертификаты',
            'active' => false
        ]
    ]),
    'groups' => collect([
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'doc',
            'category' => '1',
            'date_change' => '10 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'doc',
            'category' => '2',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'jpg',
            'category' => '3',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'jpg',
            'category' => '1',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'pdf',
            'category' => '2',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'png',
            'category' => '3',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'pdf',
            'category' => '1',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'xls',
            'category' => '2',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'zip',
            'category' => '3',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'embed',
            'category' => '1',
            'date_change' => '9 апреля 2020',
            'isFile' => true
        ]
    ])
        ->map(function ($arDocument, $key) {
            $document = new Document;

            $document->strMainId = $key;
            $document->isFile = $arDocument['isFile'];
            $document->href = $arDocument['href'];
            $document->text = $arDocument['name'];
            $document->extension = $arDocument['mime'];
            $document->categoryId = $arDocument['category'];
            $document->dateAt = "Добавлено {$arDocument['date_change']}";

            return $document;
        })
        ->groupBy(function (Document $document) {
            return $document->categoryId;
        })
        ->map(function (Collection $documents, $categoryId) {
            return [
                'id' => $categoryId,
                'documents' => $documents->values()
            ];
        })
        ->values()
        ->map(function (array $arCategory, $key) {
            $arCategory['active'] = $key === 0;

            return $arCategory;
        })
        ->values()
];

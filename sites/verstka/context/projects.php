<?php

use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\Projects\Item;

return collect([
    [
        'image' => [
            'md-src' => '/local/assets/images/index-projects/img-1-mobile.jpg',
            'src' => '/local/assets/images/index-projects/img-1.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'area' => 'район СХПК «Тепличный»',
        'fullPrice' => 'от 1 948 000 ₽',
        'price' => '57 294 ₽ | м<sup>2</sup>',
        'address' => 'г. Благовещенск, ул. Тепличная ул, 1',
        'anotherFooterText' => 'Введен в эксплуатацию'
    ],
    [
        'image' => [
            'md-src' => '/local/assets/images/index-projects/img-3-mobile.jpg',
            'src' => '/local/assets/images/index-projects/img-3.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'area' => 'район СХПК «Тепличный»',
        'address' => 'Благовещенск, ул. Амурская, 270/1',
        'deadline' => 'III квартал 2020 г.',
        'isBig' => false
    ],
    [
        'image' => [
            'md-src' => '/local/assets/images/index-projects/img-2-mobile.jpg',
            'src' => '/local/assets/images/index-projects/img-2.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'area' => 'район СХПК «Тепличный»',
        'deadline' => 'IV квартал 2020 г.'
    ],
    [
        'image' => [
            'md-src' => '/local/assets/images/index-projects/img-3-mobile.jpg',
            'src' => '/local/assets/images/index-projects/img-3.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'area' => 'район СХПК «Тепличный»',
        'fullPrice' => 'от 1 948 000 ₽',
        'address' => 'Благовещенск, ул. Амурская, 270/1',
        'isBig' => false,
        'anotherFooterText' => 'Введен в эксплуатацию'
    ]
])->map(function ($arItem) {
    $item = new Item;

    $item->image = new AdaptiveImage;
    $item->image->src = $arItem['image']['src'];
    $item->image->mdSrc = $arItem['image']['md-src'];

    $item->detailPageLink = new Link;
    $item->detailPageLink->href = '#';
    $item->detailPageLink->text = $arItem['name'];

    if (isset($arItem['area'])) {
        $item->area = $arItem['area'];
    }
    if (isset($arItem['fullPrice'])) {
        $item->fullPrice = $arItem['fullPrice'];
    }
    if (isset($arItem['price'])) {
        $item->price = $arItem['price'];
    }
    if (isset($arItem['address'])) {
        $item->address = $arItem['address'];
    }
    if (isset($arItem['deadline'])) {
        $item->deadline = $arItem['deadline'];
    }
    if (isset($arItem['anotherFooterText'])) {
        $item->anotherFooterText = $arItem['anotherFooterText'];
    }
    if (isset($arItem['isBig'])) {
        $item->isBig = $arItem['isBig'];
    }

    return $item;
});
